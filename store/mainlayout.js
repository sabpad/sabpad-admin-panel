export const state = () => ({
  drawer: true
})

export const mutations = {
  toggle(state) {
    state.drawer = !state.drawer
  },
  setDrawer(state, val) {
    state.drawer = !!val
  }
}
