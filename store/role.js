export const state = () => ({
  roles: [
    {
      title: 'Principal',
      scope: 'JNV Glt',
      _title: 'principal',
      _scope: 'jnvglt'
    },
    {
      title: 'Teacher',
      scope: 'JNV Glt',
      _title: 'teacher',
      _scope: 'jnvglt'
    },
    {
      title: 'Accountant',
      scope: 'JNV Glt',
      _title: 'accnt',
      _scope: 'jnvglt'
    }
  ],
  selected: 0
})

export const mutations = {
  select(state, i) {
    state.selected = i
    localStorage.setItem('selectedRoleIndex', i)
  }
}
